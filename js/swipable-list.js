// https://stackoverflow.com/questions/2264072/detect-a-finger-swipe-through-javascript-on-the-iphone-and-android

// var swipableList = (function () {
//
//
//     return {
//         initialize: function (selector) {
//             let touchstartX = 0
//             let touchendX = 0
//
//             const sliders = document.getElementsByClassName(selector);
//
//             for (let i = 0; i < sliders.length; i++) {
//                 let slider = sliders[i];
//
//                 function handleGesture() {
//                     if (touchendX < touchstartX) alert('swiped left!')
//                     if (touchendX > touchstartX) alert('swiped right!')
//                 }
//
//                 slider.addEventListener('touchstart', e => {
//                     touchstartX = e.changedTouches[0].screenX;
//                 })
//
//                 slider.addEventListener('touchend', e => {
//                     touchendX = e.changedTouches[0].screenX;
//                     handleGesture();
//                 })
//             }
//         }
//
//     };
// })();


var setupSwipableList = function (onLeftSwipe, onRightSwipe) {

    let $list = $('.listView');


    // Cache a reference to the collection of list items
    // var $items = $list.find('.listViewItem');
    //
    // Make the items equal heights
    // var sizeItems = function() {
    //     var ih = Math.floor($(window).height() / $items.length);
    //     $items.height(ih).find('a').css('line-height',ih + 'px');
    // }
    // Ensure the items handle screen reszing
    // $(window).on('resize', sizeItems).trigger('resize');


    /**
     * Set up a swipe gesture.  Hammer has a 'swipe' event but
     * it's not particularly reliable in my experience, so I fake it
     * by detecting drags and deciding if it was a swipe based on the distance.
     *
     * Note that this just takes distance into account - you could do a slow drag
     * and it would still get picked up as a 'swipe' using this method. You would
     * need to get time deltas between the drag start and end if you wanted to check
     * the speed of the drag as well.
     **/

        // var self = this;

        // We're going to bind the drag event listener to the list container. Hammer doesn't
        // understand jQuery collections so we need to use the raw DOM element reference.
    let listContainer = $list[0];
    // Events will bubble up the DOM and fire on all the elements we drag over, but
    // we only want to react to the event if the user performs it inside a list item.
    // This var stores a reference to selector we'll be checking for.
    // let targetSelectorOther = '.listView .listViewItemContent';

    // To prevent errant taps/clicks being interpreted as drags, we only want to
    // react to drags of 100px or more.
    let threshold = 100;
    let MaxOffset = 120;

    if (listContainer == null) {
        return null;
    }

    let getTargetEls = function (mgr, target) {

        let targetSelector = '.listView .listViewItem';
        // Some vars we will use later to see if we dragged the right element
        let isDraggingTarget = false,
            isDraggingChildOfTarget = false,
            draggedParents = false;

        // Get the element on which the 'drag' event has just fired
        // and wrap it in a jQuery object for convenience
        let dragged = $(target);

        // Check to see if we dragged the element we want directly
        isDraggingTarget = dragged.is(targetSelector);// || dragged.is(targetSelectorOther);

        // If we havent' dragged the element we want, we might be dragging
        // one of its children (e.g. a <span> inside an <li>), so check that
        // the element doesn't have a parent in the DOM which matches what we
        // want
        if (!isDraggingTarget) {
            draggedParents = dragged.parents(targetSelector);
            isDraggingChildOfTarget = draggedParents.length;
        }

        // If we're dragging our target or one of its children, we want to proceed
        if (isDraggingTarget || isDraggingChildOfTarget) {

            // We want to work just our target element - that's either the one we dragged
            // directly, or the parent we discovered was actually the target.
            let el = (isDraggingChildOfTarget) ? draggedParents : dragged;
            return el;
        }

        return null;
    };

    // listView
    // Now we bind the event to the list container.
    let manager = new Hammer.Manager(listContainer);

    // Create a recognizer
    // manager.add(new Hammer.Swipe());
    manager.add(new Hammer.Pan({direction: Hammer.DIRECTION_HORIZONTAL, threshold: 10}));

    manager.on("pancancel", function (e) {

        let el = getTargetEls(manager, e.target);
        if (el !== null) {
            el[0].style.transform = "";
        }
    });

    // manager.on("panup panup", function (e) {
    //    
    // });
    manager.on("panleft panright", function (e) {
    // manager.on("swipe", function (e) {
        
        let el = getTargetEls(manager, e.target);
        if (el !== null) {
            let offset = 0;
            if (el.hasClass("prompt-action-left")) {
                offset = -MaxOffset;
            } else if (el.hasClass("prompt-action-right")) {
                offset = MaxOffset;
            }

            let dragDistance = e.deltaX + offset;

            let translate3d = "";
            if (dragDistance < 0) {
                if (dragDistance < -MaxOffset) {
                    translate3d = 'translate3d(' + (0 - MaxOffset) + 'px)';
                } else {
                    translate3d = 'translate3d(' + dragDistance + 'px, 0, 0)';
                }
            } else {

                if (dragDistance > MaxOffset) {
                    translate3d = 'translate3d(' + (MaxOffset) + 'px)';
                } else {
                    translate3d = 'translate3d(' + dragDistance + 'px, 0, 0)';
                }
            }

            el[0].style.transform = translate3d;
        }
    });

    manager.on('panend', function (e) {
        // alert(1);
        let el = getTargetEls(manager, e.target);
        if (el !== null) {

            el[0].style.transform = "";
            let dragDistance = e.deltaX;
            let absDragDistance = Math.abs(dragDistance);

            if (absDragDistance > threshold) {
                if (el.hasClass("prompt-action-left")) {
                    if (dragDistance > 0) {
                        el.removeClass('prompt-action-left');
                    }
                } else if (el.hasClass("prompt-action-right")) {
                    if (dragDistance < 0) {

                        el.removeClass('prompt-action-right');
                    }
                } else {
                    el.addClass(dragDistance > 0 ? "prompt-action-right" : "prompt-action-left");

                    let func = dragDistance > 0 ? onRightSwipe : onLeftSwipe;
                    if (func != null) {
                        func(el[0]);
                    }
                }
            }
        }
    });
}